function signalHandler() {

    function handle(signal) {
        console.log(`Received ${signal}`);
        process.exit(0);
    }

    process.on('SIGINT', handle);
    process.on('SIGTERM', handle);

}

module.exports = signalHandler;
